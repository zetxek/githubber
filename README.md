# README #

This is an Android app to showcase some Java (8) code, and playing around with some Android libraries:

* rxjava
* retrofit
* gson
* picasso
* recyclerview
* cardview

It downloads some data from Github's API and shows it on screen.

### What could be improved? ###

* Writing tests (and/or updating the existing ones)
* Replace SimpleDateFormatter in the Application (https://medium.com/@tk512/be-careful-with-simpledateformat-8603e108dc8d#.9yh7my3tq)
* Nicer layouts
* Some view animations