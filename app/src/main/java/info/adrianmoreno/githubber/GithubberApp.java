package info.adrianmoreno.githubber;

import android.app.Application;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by zetxek on 22/11/2016.
 */

public class GithubberApp extends Application {
    public static final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

}
