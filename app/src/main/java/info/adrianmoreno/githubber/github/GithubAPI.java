package info.adrianmoreno.githubber.github;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import info.adrianmoreno.githubber.model.github.Repo;
import info.adrianmoreno.githubber.model.github.SearchResult;
import info.adrianmoreno.githubber.model.github.User;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by zetxek on 15/11/2016.
 */

public class GithubAPI {

    public static final String BASE_URL = "https://api.github.com/";
    public Retrofit retrofit;
    public GitHubService github;

    public interface GitHubService {

        @GET("search/users?q=tom+followers:>100")
        Observable<SearchResult> getTopUsers();

        @GET("users/{user}")
        Observable<User> getUser(
                @Path("user") String user
        );

        @GET("users/{user}/repos")
        Observable<List<Repo>> getUserRepos(
                @Path("user") String user
        );


    }

    public interface GitHubUser {
        @GET("users/{user}")
        Observable<JsonObject> getUser(@Path("user") String user);
    }

    public interface GitHubEvents {
        @GET("users/{user}/events")
        Observable<JsonArray> listEvents(@Path("user") String user);
    }

    public GithubAPI() {

        Gson gson = new GsonBuilder()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        github = retrofit.create(GitHubService.class);


    }

    /*public SearchResult getUsers() throws IOException {
        return github.getTopUsers().execute().body();
    }

    public SearchResultUser getRandomUser() throws IOException {
        SearchResult topUsers = getUsers();
        Random randomGenerator = new Random();
        SearchResultUser randomUser = topUsers.items.get(randomGenerator.nextInt(topUsers.items.size()));
        Timber.d("Random user %s", randomUser.login);
        return randomUser;
    }*/
}
