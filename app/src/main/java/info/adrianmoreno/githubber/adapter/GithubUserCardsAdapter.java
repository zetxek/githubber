package info.adrianmoreno.githubber.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.List;

import info.adrianmoreno.githubber.R;
import info.adrianmoreno.githubber.model.github.User;

/**
 * Created by zetxek on 22/11/2016.
 */
public class GithubUserCardsAdapter extends RecyclerView.Adapter<GithubUserCardsAdapter.GithubUserViewHolder> {

    public List<User> usersList;

    public void addUser(User u) {
        usersList.add(u);
        notifyItemInserted(usersList.size());
    }

    public void removeAll() {
        Iterator<User> it = usersList.iterator();
        while (it.hasNext()){
            User u = it.next();
            int pos = usersList.indexOf(u);
            it.remove();
            notifyItemRemoved(pos);
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class GithubUserViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtUserName;
        public TextView txtUserLogin;
        public TextView txtUserRepoCount;
        public TextView txtUserURL;
        public TextView txtUserRepos;
        public ImageView imgUserAvatar;


        public GithubUserViewHolder(View v) {
            super(v);
            txtUserLogin = (TextView) v.findViewById(R.id.card_element_login);
            txtUserName = (TextView) v.findViewById(R.id.card_element_username);
            txtUserRepoCount = (TextView) v.findViewById(R.id.card_element_repo_count);
            txtUserURL = (TextView) v.findViewById(R.id.card_element_url);
            txtUserRepos  = (TextView) v.findViewById(R.id.card_element_repos_list);
            imgUserAvatar = (ImageView) v.findViewById(R.id.card_element_avatar);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public GithubUserCardsAdapter(List<User> myDataset) {
        usersList = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GithubUserViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.github_user_card, parent, false);
        // set the view's size, margins, paddings and layout parameters
        GithubUserViewHolder vh = new GithubUserViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(GithubUserViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.txtUserName.setText(usersList.get(position).name);
        holder.txtUserLogin.setText(usersList.get(position).login);
        holder.txtUserURL.setText(usersList.get(position).url);
        holder.txtUserRepoCount.setText("# " + usersList.get(position).publicRepos);
        holder.txtUserRepos.setText(Html.fromHtml(usersList.get(position).getReposlist()));
        Picasso.with(holder.imgUserAvatar.getContext()).load(
                usersList.get(position).avatarUrl).into(holder.imgUserAvatar
        );

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return usersList.size();
    }
}



