package info.adrianmoreno.githubber.model;

/**
 * Created by zetxek on 22/11/2016.
 */

public class TimeMeasurement {
    public long start = 0;
    public long end = 0;

    public long getTimeDiff(){
        return end - start;
    }
}
