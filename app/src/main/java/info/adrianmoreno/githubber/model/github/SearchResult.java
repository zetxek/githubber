package info.adrianmoreno.githubber.model.github;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zetxek on 17/11/2016.
 */

public class SearchResult {

    @SerializedName("total_count")
    @Expose
    public int total_count;

    @SerializedName("incomplete_results")
    @Expose
    public boolean incomplete_results;

    @SerializedName("items")
    @Expose
    public List<SearchResultUser> items;
}
