package info.adrianmoreno.githubber.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.adrianmoreno.githubber.R;
import info.adrianmoreno.githubber.adapter.GithubUserCardsAdapter;
import info.adrianmoreno.githubber.github.GithubAPI;
import info.adrianmoreno.githubber.model.TimeMeasurement;
import info.adrianmoreno.githubber.model.github.Repo;
import info.adrianmoreno.githubber.model.github.User;
import info.adrianmoreno.githubber.util.rxjava.RxErrorHandlingCallAdapterFactory;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.txtCall1)
    TextView txtCall1;
    @BindView(R.id.txtCall2)
    TextView txtCall2;
    @BindView(R.id.txtCallsOverview)
    TextView txtCallsOverview;

    @BindView(R.id.my_recycler_view)
    RecyclerView recyclerView;
    private GithubUserCardsAdapter usersAdapter;
    private RecyclerView.LayoutManager layoutManager;

    List<TimeMeasurement> times = new ArrayList<>();
    Animation menuRotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        times.add(new TimeMeasurement()); // call 1
        times.add(new TimeMeasurement()); // call 2

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        recyclerView.setItemAnimator(new SlideInLeftAnimator());
        recyclerView.getItemAnimator().setAddDuration(1000);


        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        usersAdapter = new GithubUserCardsAdapter(new ArrayList<>());
        SlideInBottomAnimationAdapter alphaAdapter = new SlideInBottomAnimationAdapter(usersAdapter);
        alphaAdapter.setDuration(1000);
        recyclerView.setAdapter(alphaAdapter);

        menuRotation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
        menuRotation.setRepeatCount(Animation.INFINITE);

        downloadUsersInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        supportInvalidateOptionsMenu();
    }

    @OnClick(R.id.btnSearch)
    public void clickBtnSearch() {
        Timber.d("Clicked in BtnSearch.");
        downloadUsersInfo();
    }

    private void downloadUsersInfo() {

        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String user1 = SP.getString(getString(R.string.pref_user1), "zetxek");
        String user2 = SP.getString(getString(R.string.pref_user2), "JakeWharton");


        txtCall1.setText("-");
        txtCall2.setText("-");
        txtCallsOverview.setText("Downloading...");

        usersAdapter.removeAll();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GithubAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build();

        GithubAPI.GitHubService gitHubService = retrofit.create(GithubAPI.GitHubService.class);

        Observable<List<Repo>> userReposObservable1 = gitHubService.getUserRepos(user1);
        Observable<User> userDataObservable1 = gitHubService.getUser(user1);


        Observable<User> userObservable1 = Observable.zip(userReposObservable1, userDataObservable1, (repos, user) -> {
            user.repos = repos;
            return user;
        })
                .doOnSubscribe(() -> times.get(0).start = System.currentTimeMillis())
                .doOnUnsubscribe(() -> times.get(0).end = System.currentTimeMillis())
                ;

        Observable<List<Repo>> userReposObservable2 = gitHubService.getUserRepos(user2);
        Observable<User> userDataObservable2 = gitHubService.getUser(user2);
        Observable<User> userObservable2 = Observable.zip(userReposObservable2, userDataObservable2, (repos, user) -> {
            user.repos = repos;
            return user;
        })
                .doOnSubscribe(() -> times.get(1).start = System.currentTimeMillis())
                .doOnUnsubscribe(() -> times.get(1).end = System.currentTimeMillis())
                ;

        userObservable1.zipWith(userObservable2,
                        (resultUser1, resultUser2) -> {
                            Timber.d("Users info: %s, %s", resultUser1, resultUser2);
                            List<User> users = new ArrayList();
                            users.add(resultUser1);
                            users.add(resultUser2);
                            return users;
                        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> menuRotation.cancel())
                .subscribe(output -> {
                    Timber.d("Execution ended");

                    ArrayList<User> users = (ArrayList<User>)output;
                    for (User u : users){
                        usersAdapter.addUser(u);
                    }

                    long diff1 = times.get(0).getTimeDiff();
                    long diff2 = times.get(1).getTimeDiff();

                    txtCall1.setText(String.format("%d ms", diff1));
                    txtCall2.setText(String.format("%d ms", diff2));

                    String callsOverview = "User call ";
                    if (times.get(1).end > times.get(0).end){
                        callsOverview += "#1";
                    }else{
                        callsOverview += "#2";
                    }
                    long absDiff = Math.abs(diff1 - diff2);
                    callsOverview += String.format(" waited for the other call to finish (%d ms).\n", absDiff);


                    txtCallsOverview.setText(callsOverview);

                }, error -> {
                    Timber.e(error);
                    Toast.makeText(MainActivity.this, "There has been an error", Toast.LENGTH_LONG).show();
                    txtCall1.setText("-");
                    txtCall2.setText("-");
                    txtCallsOverview.setText(String.format("Error: %s", error.getLocalizedMessage()));
                    menuRotation.cancel();
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.main_menu, menu);

        ImageView refreshMenuItem = (ImageView) menu.findItem(R.id.menu_item_reload).getActionView();
        if (refreshMenuItem != null) {
            refreshMenuItem.setImageResource(android.R.drawable.ic_popup_sync);
            refreshMenuItem.setOnClickListener(view -> {
                view.startAnimation(menuRotation);
                downloadUsersInfo();
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_preferences:
                if (menuRotation != null) {
                    menuRotation.cancel();
                }
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
