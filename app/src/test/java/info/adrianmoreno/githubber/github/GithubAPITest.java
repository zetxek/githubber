package info.adrianmoreno.githubber.github;

import android.util.Log;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import info.adrianmoreno.githubber.model.github.SearchResult;
import info.adrianmoreno.githubber.model.github.SearchResultUser;
import timber.log.Timber;

/**
 * Created by zetxek on 17/11/2016.
 */

public class GithubAPITest {

    @Test
    public void getUsersTest() throws IOException {
        GithubAPI githubAPI = new GithubAPI();
        SearchResult users = githubAPI.getUsers();
        Assert.assertNotEquals(users.items.size(), 0);
        Timber.d("Received %d results", users.items.size());
    }

    @Test
    public void getRandomUser() throws IOException {
        GithubAPI githubAPI = new GithubAPI();
        SearchResultUser randomUser = githubAPI.getRandomUser();
        Log.d("getRandomUser", "Received random user: " + randomUser);
        Assert.assertNotEquals(randomUser, null);
    }



}
